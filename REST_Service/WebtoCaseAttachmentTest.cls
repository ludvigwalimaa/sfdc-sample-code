@isTest 
private class WebtoCaseAttachmentTest{ 
    
    @isTest static void testdoPost(){ 
        
        
        String JsonMsg = '{"Language":"en","Name":"Testperson","Email":"test@email.com","Subject":"CaseSubject","Message":"CaseMessage","Files":[{"Name":"image1.png","Ext":"image/png","Base64":"iVBORw0KGgoAAAANSUhEUgAAABoAAAAVCAIAAADXfmlMAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAACpSURBVDhPY3gKBk+ePHkMBo8ePXr48OEDMLh///49GLhLHAAZBzELYtCOw6eiG6fLh5cxO6cxOKaSjOBm3b57L7Z5BpMTWabAEcSsO3fvWWW1osuRgYDGAf0Y3TQdXYI8BHTazqOnKfUjHAGdltg2C12UbARMEMpRFeiiZCOgcayuGeiiZCNgWkUXogRR2ThgBkIXogRR2ThQvkUTogSNGkc+GjnGOaYCANco2EJ0M+tvAAAAAElFTkSuQmCC"},{"Name":"image2.png","Ext":"image/png","Base64":"iVBORw0KGgoAAAANSUhEUgAAABUAAAAaCAIAAADXI4AUAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAoSURBVDhPY/Qon8hAAWCC0uSCUf2UgVH9lIFR/ZSBUf2UgZGtn4EBADUeAYQfV8kAAAAAAElFTkSuQmCC"}]}' ;
        
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.requestURI          = '/services/apexrest/WebtoCaseAttachment'; 
        req.httpMethod          = 'POST';
        req.requestBody         = Blob.valueOf(JsonMsg);
        RestContext.request     = req;
        RestContext.response    = res;
        
        
        List<WebtoCaseAttachment.customerAttachment> ca_list = new List<WebtoCaseAttachment.customerAttachment>();
        List<WebtoCaseAttachment.customerAttachment> ca_list_error = new List<WebtoCaseAttachment.customerAttachment>();
        WebtoCaseAttachment.customerAttachment ca = new WebtoCaseAttachment.customerAttachment();
        ca.Name = 'image1.png';
        ca.Ext = 'image/png';
        ca.Base64 = 'iVBORw0KGgoAAAANSUhEUgAAABoAAAAVCAIAAADXfmlMAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAACpSURBVDhPY3gKBk+ePHkMBo8ePXr48OEDMLh///49GLhLHAAZBzELYtCOw6eiG6fLh5cxO6cxOKaSjOBm3b57L7Z5BpMTWabAEcSsO3fvWWW1osuRgYDGAf0Y3TQdXYI8BHTazqOnKfUjHAGdltg2C12UbARMEMpRFeiiZCOgcayuGeiiZCNgWkUXogRR2ThgBkIXogRR2ThQvkUTogSNGkc+GjnGOaYCANco2EJ0M+tvAAAAAElFTkSuQmCC';
        ca_list.add(ca);
        
        WebtoCaseAttachment.customerAttachment ca2 = new WebtoCaseAttachment.customerAttachment();
        ca2.Name = 'image2.png';
        ca2.Ext = 'image/png';
        ca2.Base64 = 'iVBORw0KGgoAAAANSUhEUgAAABUAAAAaCAIAAADXI4AUAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAoSURBVDhPY/Qon8hAAWCC0uSCUf2UgVH9lIFR/ZSBUf2UgZGtn4EBADUeAYQfV8kAAAAAAElFTkSuQmCC';
        ca_list.add(ca2);
        
        WebtoCaseAttachment.customerAttachment ca3 = new WebtoCaseAttachment.customerAttachment();
        ca3.Name = 'image2.png';
        ca3.Ext = 'image/error';
        ca3.Base64 = 'iVBORw0KGgoAAAANSUhEUgAAABUAAAAaCAIAAADXI4AUAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAoSURBVDhPY/Qon8hAAWCC0uSCUf2UgVH9lIFR/ZSBUf2UgZGtn4EBADUeAYQfV8kAAAAAAElFTkSuQmCC';
        ca_list_error.add(ca3);
            
        
        WebtoCaseAttachment.doPost('en', 'Testperson', 'test@email.com', 'CaseSubject', 'CaseMessage', 'Web', ca_list);  //Will succeed.      
        WebtoCaseAttachment.doPost('en', 'Testperson', 'test', 'CaseSubject', 'CaseMessage', 'Web', ca_list_error); //Will invoke error.

        
        Test.stopTest();
         
    } 
     
}
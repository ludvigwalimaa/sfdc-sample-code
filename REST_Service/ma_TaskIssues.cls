/**
	Service to create a case related to a Work Order if a Hemfridare(Contact) for example accidentally breaks something during the work shift.
	Possible to upload image and add description from MedarbetarApp that is attached to the case.
	POST	: Create Case in Salesforce and attach image and description regarding the issue that occurred during work.
	Used by medarbetarApp.
	
	@author Ludvig Wälimaa
*/
@RestResource(urlMapping='/Task/Issues')
global with sharing class ma_TaskIssues {
	 
    
    /**
    
    Creates a Case with Attachment related to a Work Order.
	@param itemId 		The id of the WorkOrderLineItem for which the Hemfridare is working on. 
	@param MIME 		The format of the image
    @param imgName 		The name of the image 
    @param description 	The description of the issue 
    @param employeeId 	The Hemfridare(Contact) that cause the issue 

	*/
    @HttpPost
    global static void doPost(String base64) {
        RestRequest req 			= RestContext.request;
        RestResponse res 			= RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        String jsonResponse			= '';
        String itemId				= req.params.get('id');   // Try with 1WLP00000004DGbOAM
		String MIME  				= req.params.get('mime');  //image/png
        String imgName 				= req.params.get('imgName'); //headerColor.png
        String description			= req.params.get('desc');
        String employeeId			= req.params.get('empId'); //003P0000018oojH
        

        //If parameters id, mime, imgName, desc or empId are empty
        if(itemId == ''|| MIME == ''|| imgName == ''|| description == ''|| employeeId == '') {
            res.statusCode 		= 400;
            jsonResponse 		= '{"response": {"type": "BAD_REQUEST", "message": "MissingRequiredValue for parameter: id, mime, base64, imgName, desc or empId""}}';
            res.responseBody 	= blob.valueOf(jsonResponse);
            return;
        }
        
        //If parameters id, mime, imgName, desc or empId are missing from query
        if(itemId == null || MIME == null  || imgName == null || description == null || employeeId == null) {
            res.statusCode 		= 400;
            jsonResponse 		= '{"response": {"type": "BAD_REQUEST", "message": "MissingRequiredQueryParameter: id, mime, base64, imgName, desc or empId"}}';
            res.responseBody	= blob.valueOf(jsonResponse);
            return;
        }


        List <WorkOrderLineItem> woli_list = new List<WorkOrderLineItem>();
        woli_list = [SELECT id, 
							Status, 
							WorkOrder.Id, 
							WorkOrder.Delivery_Address__r.Name, 
							WorkOrder.Delivery_Address__c, 
							WorkOrder.Account.Id 
							FROM WorkOrderLineItem 
							WHERE id = :itemId LIMIT 1];
        
        List <Contact> con_list = new List<Contact>();
        con_list = [SELECT id, Area__r.Area_Manager__r.Id FROM Contact WHERE id = :employeeId LIMIT 1];
        
        
        //If there is no WorkOrderLineItem tied to the input itemId
        if(woli_list.size() <= 0) {
        	res.statusCode 		= 404;
            jsonResponse 		= '{"response": {"type": "NOT_FOUND", "message": "There is no Task tied to that id"}}';
            res.responseBody 	= blob.valueOf(jsonResponse);
            return; 
        }

        //If there is no Employee tied to the input empId
        if(con_list.size() <= 0) {
            res.statusCode 		= 404;
            jsonResponse 		= '{"response": {"type": "NOT_FOUND", "message": "There is no Employee tied to that id"}}';
            res.responseBody 	= blob.valueOf(jsonResponse);
            return; 
        }

        //Creating new Case
        Case c 						= new Case();
        c.Description 				= description;
		c.Hemfridare__c 			= employeeId;
		c.Work_Order__c 			= woli_list.get(0).WorkOrder.Id;
		c.AccountId					= woli_list.get(0).WorkOrder.Account.Id;
        c.OwnerId 					= con_list.get(0).Area__r.Area_Manager__r.Id;
		c.RecordTypeId              = RecordTypeHelper.CASE_DAMAGE; //Skada
		c.Delivery_Address__c		= woli_list.get(0).WorkOrder.Delivery_Address__c;
        c.Subject                   = 'Skada: ' + woli_list.get(0).WorkOrder.Delivery_Address__r.Name;
        
        //Inserting the Case to retrieve a Case Id.
        try{
            insert c;
        }catch(Exception ex){
            res.statusCode = 500;
            jsonResponse = '{"response": {"type": "INTERNAL_SERVER_ERROR", "message": "' + ex + '"}}';
            res.responseBody = blob.valueOf(jsonResponse);
            return;
        }

        //Creating new Attachment to the previously created Case
		Attachment a 	= new Attachment();
        a.Body 			= EncodingUtil.base64Decode(base64);
        a.Name 			= imgName;
        a.ContentType 	= MIME;
        a.ParentId 		= c.Id;

        try{
            insert a;
            res.statusCode = 201;
            jsonResponse = '{"response": {"type": "CREATED", "message": "New incident with attachment inserted."}}';
           	res.responseBody = blob.valueOf(jsonResponse);           
            return;  
        } catch(Exception ex){
            res.statusCode = 500;
            jsonResponse = '{"response": {"type": "INTERNAL_SERVER_ERROR", "message": "' + ex + '"}}';
            res.responseBody = blob.valueOf(jsonResponse);
            return;
        }
             
    }
  

}
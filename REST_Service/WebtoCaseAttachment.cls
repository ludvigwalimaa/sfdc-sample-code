@RestResource(urlMapping='/Case/*')
global without sharing class WebtoCaseAttachment {
       
    global class customerAttachment{
        global String Name      {get; set;}
        global String Ext       {get; set;}
        global String Base64    {get; set;}
    }
  
  @HttpPost
    global static void doPost(String Language, String Name, String Email, String Subject, String Message, String Origin, List<customerAttachment> Files) {
        
        String jsonResponse = '';
        RestResponse res = RestContext.response;
       
        try{ 
            Case c = new Case();
            c.Form_Language__c  = Language;
            c.SuppliedName      = Name;
            c.SuppliedEmail     = Email;
            c.Subject           = Subject;
           // c.SuppliedPhone   = Phone;
           // c.SuppliedCompany = Company;
            c.Description       = Message;
            c.Origin            = Origin;
           
            
            insert c;

            for(customerAttachment ca : Files){
                Attachment a = new Attachment();
                a.Name          = ca.Name;
                a.ContentType   = ca.Ext;
                a.Body          = EncodingUtil.base64Decode(ca.Base64);
                a.ParentId      = c.Id;
                insert a;  
            } 
            res.statusCode = 201;
            jsonResponse = '{"response": {"status": "Success", "message": "Case with potential attachments uploaded in Salesforce" }}';
            res.responseBody = blob.valueOf(jsonResponse);
            return;            
        }catch(Exception ex){
            res.statusCode = 400;
            jsonResponse = '{"response": {"status": "Failure", "message": "' + ex + '"}}';
            res.responseBody = blob.valueOf(jsonResponse);
            return;
        }      
               
          
    }
}
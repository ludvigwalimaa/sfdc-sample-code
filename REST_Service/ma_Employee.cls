/**
	Service to retrieve and update a Contact and related data. 
	GET		: Retrieve a Contact(Employee) and related data.
	PATCH	: Update the language of a Contact(Employee)
	Used by MedarbetarApp.
	
	@author Ludvig Wälimaa - Deloitte
*/
@RestResource(urlMapping='/Employee')
global with sharing class ma_Employee {
	  
    global class Employee{
        global String Id		 		{get; set;}
        global String FirstName 		{get; set;}
        global String LastName 			{get; set;}
        global String Address			{get; set;} 
        global String MobilePhone 		{get; set;} 
        global String Email 			{get; set;} 
        global String ImageId			{get; set;}
        global List<String> Languages 	{get; set;}
        global Manager Manager		 	{get; set;}
    } 
   
    //Manager = OC in Salesforce
    global class Manager{
        global String Id		 		{get; set;}
        global String FirstName 		{get; set;}
        global String LastName 			{get; set;}
        global String MobilePhone 		{get; set;}
		global String Email				{get; set;}
      	global String ImageId 			{get; set;} 
    }
    
    /**
    
    Returns a json response with a Contact and its related data. 
	@param personaNumber The personal number of a Contact 

	*/
    @HttpGet
    global static void doGet() {
        RestRequest req 		= RestContext.request;
        RestResponse res 		= RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        String jsonResponse		= '';
        String personalNumber	= req.params.get('personal_number');  // Try with 19850621-7846
        
        //If personalNumber = empty
        if(personalNumber == '') {
            res.statusCode 		= 400;
            jsonResponse 		= '{"response": {"type": "BAD_REQUEST", "message": "MissingRequiredValue for parameter: personal_number"}}';
            res.responseBody 	= blob.valueOf(jsonResponse);
            return;
        }
        
        //If personalNumber missing from query
        if(personalNumber == null) {
            res.statusCode 		= 400;
            jsonResponse 		= '{"response": {"type": "BAD_REQUEST", "message": "MissingRequiredQueryParameter: personal_number"}}';
            res.responseBody	= blob.valueOf(jsonResponse);
            return;
        }
        
		//Fetching Contact
		List<Contact> con_list 	= new List<Contact>();
        con_list = [SELECT 
                    	id,
                    	Name,
                    	MobilePhone,
                    	Email,
                    	Street_Address__c,
                    	Zip_Code__c,
                    	City__c,
                    	Image_Id__c,
                    	Languages__c,
                    	Area__r.Area_Manager__r.Id,
                        Area__r.Area_Manager__r.FirstName,
                        Area__r.Area_Manager__r.LastName,
                    	Area__r.Area_Manager__r.MobilePhone,
                    	Area__r.Area_Manager__r.Smallphotourl,
						Area__r.Area_Manager__r.Email
                        FROM Contact
                        WHERE Personal_Number__c = :personalNumber LIMIT 1];

 
        //If no Contact tied to input id
        if(con_list.size() <= 0){
        	res.statusCode 		= 404;
            jsonResponse 		= '{"response": {"type": "NOT_FOUND", "message": "There is no Employee tied to that id."}}';
            res.responseBody 	= blob.valueOf(jsonResponse);
            return; 
        }
        
        //Placeholder list
        List<Employee> emp_list = new List<Employee>();
        
        for(Contact c : con_list){
            Employee e 		= new Employee();
            e.Id 			= c.Id;
            e.MobilePhone	= c.MobilePhone;
            //Fetching Name field and splitting cause some Contacts have full name in LastName field. 
            String empName	= c.Name;
            e.FirstName 	= empName.substring(0, empName.indexOf(' '));
            e.LastName 		= empName.substring(empName.indexOf(' ') + 1, empName.length());
            e.Email 		= c.Email;
            e.ImageId		= c.Image_Id__c;

            //Address
            e.Address = c.Street_Address__c + ', ' + c.Zip_Code__c + ' ' + c.City__c;
                       
            //Languages
            if(c.Languages__c != null){
                e.Languages 		= new List<String>();
                String s 			= c.Languages__c;
                String[] lang_list 	= s.split(';');
                for(Integer l = 0; l<lang_list.size(); l++){
                    e.Languages.add(lang_list[l]);
                }
            }
            
            //Manager
            Manager m 			= new Manager();
            m.Id 				= c.Area__r.Area_Manager__r.Id;
            /*
            String managerName 	= c.Area__r.Area_Manager__r.Name;
            if(managerName.indexOf(' ') > 0 ){
                m.FirstName 	= managerName.substring(0, managerName.indexOf(' '));  
            }else{
                m.FirstName = ' ';
            }
            m.LastName 			= managerName.substring(managerName.indexOf(' ') + 1, managerName.length());
			*/
            m.FirstName = c.Area__r.Area_Manager__r.FirstName;
            m.LastName = c.Area__r.Area_Manager__r.LastName;
            m.MobilePhone 		= c.Area__r.Area_Manager__r.MobilePhone;
			m.Email				= c.Area__r.Area_Manager__r.Email;
            m.ImageId			= m.Id;
           	e.Manager = m;
      	
            emp_list.add(e);
        }

        //A Contact was found with input personal number. An Employee-response have been put together and is ready to be returned.
        try{
            res.statusCode = 200;
            jsonResponse = Json.serialize(emp_list); 
            res.responseBody = blob.valueOf(jsonResponse);           
            return;  
        } catch(Exception ex){
            res.statusCode = 500;
            jsonResponse = '{"response": {"type": "INTERNAL_SERVER_ERROR", "message": "' + ex + '"}}';
            res.responseBody = blob.valueOf(jsonResponse);
            return;
        }
             
    }
  
    /**
    Updates the languages a of a Contact.

	@param personalNumber The personal number of a Contact 
	@paran inputLanguages The languages that will be added to the Contact. Separate multiple languages with comma.
	
	*/
	@HttpPatch
    global static void doPatch(){
        RestRequest req 		= RestContext.request;
        RestResponse res 		= RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        String jsonResponse		= '';
        String personalNumber	= req.params.get('personal_number');   // Try with 19850621-7846
        String inputLanguages	= req.params.get('lang');

        //If personal_number/lang = empty
        if(personalNumber == '' || inputLanguages == '') {
            res.statusCode 		= 400;
            jsonResponse 		= '{"response": {"type": "BAD_REQUEST", "message": "MissingRequiredValue for parameter: personal_number or lang"}}';
            res.responseBody 	= blob.valueOf(jsonResponse);
            return;
        }
        
        //If personal_number/lang missing from query
        if(personalNumber == null || inputLanguages == null) {
            res.statusCode 		= 400;
            jsonResponse 		= '{"response": {"type": "BAD_REQUEST", "message": "MissingRequiredQueryParameter: personal_number or lang"}}';
            res.responseBody	= blob.valueOf(jsonResponse);
            return;
        } 
        
        List<Contact> con_list = new List<Contact>();
        con_list = [SELECT 
                    id, 
                    Languages__c 
                    FROM Contact 
                    WHERE Personal_Number__c = :personalNumber 
                    LIMIT 1];
        
        //If no Contact tied to input personal number
        if(con_list.size() <= 0){
        	res.statusCode 		= 404;
            jsonResponse 		= '{"response": {"type": "NOT_FOUND", "message": "There is no Employee tied to that personal number"}}';
            res.responseBody 	= blob.valueOf(jsonResponse);
            return; 
        }
        
        //Adding new languages to the Contact
        Contact employee 				= con_list.get(0);
       	String contactCurrentLanguages 	= employee.Languages__c;
        String[] lang_split_list = inputLanguages.split(',');
        for(Integer i = 0; i<lang_split_list.size(); i++){
            if(!contactCurrentLanguages.contains(lang_split_list[i])){
                contactCurrentLanguages = contactCurrentLanguages + ';' +lang_split_list[i];
            }   
        }
        employee.Languages__c = contactCurrentLanguages;
		
        
       try{
            upsert employee;
            res.statusCode = 200;
            jsonResponse = '{"response": {"type": "OK", "message": "Employee updated. New language(s) upserted."}}'; 
           	res.responseBody = blob.valueOf(jsonResponse);           
            return;  
        } catch(Exception ex){
            res.statusCode = 500;
            jsonResponse = '{"response": {"type": "INTERNAL_SERVER_ERROR", "message": "' + ex + '"}}';
            res.responseBody = blob.valueOf(jsonResponse);
            return;
        }  
        
    }
}
/**
    
    Test class for ma_Employee. 
	@author Ludvig Wälimaa - Deloitte

	*/
@isTest
public class ma_Employee_test {
    
    private static User u;
    private static Contact con;
    private static String endpoint = '';
    
    /**
	 Setting up data. Creating OC(Manager), Hemfridare(Employee) and Area. 
	*/
    private static void setup(){
        //Fetching OC (Manager)
        u = [SELECT id, Name, FirstName, LastName FROM User where FirstName != null AND LastName != null LIMIT 1];
	
        //Creating Areas via factory
        List<Area__c> a_list = TestMetadataFactory.createAreaMetaData();
        a_list.get(0).Area_Manager__c = u.Id;
        upsert a_list.get(0);
        
        //Creating Hemfridare(Employees) via factory
        List<Contact> c_list = TestDataFactory.createHemfridare(2, a_list.get(0));
        con = c_list.get(0);
        
        //Adding data the factory is not supporting
        con.FirstName = 'FirstName';
        con.Personal_Number__c = '19890101-8310';
        con.Languages__c = 'sv';
        upsert con;

        //Setting api endpoint
        setEndpoint();
    }

    /**
     Retrieves the base url of Salesforce and builds the full api url.
    */
    private static void setEndpoint(){
        //Get domain + apiurl.
        String domain = Url.getSalesforceBaseUrl().toExternalForm();
		String api = '/services/apexrest/Employee';
        endpoint = domain + api;
    }
    
    /**
		Correct call. Returning 200 Success + json response.
    */
    @isTest
    public static void testGetEmployeeCorrect(){
        
        //Setup
        setup();
		
        //Build request
        RestRequest request = new RestRequest();
        request.requestUri = endpoint;
        request.params.put('personal_number', con.Personal_Number__c);
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = new RestResponse();
        
        ma_Employee.doGet();

		//Asserts
		System.assertEquals(200, RestContext.response.statusCode);    
    }
    
    /**
		Incorrect call. Returning 404 Not found. Input personal number not tied to any employee in Salesforce.
    */
    @isTest
    public static void testGetEmployeeNoContact(){
        
        //Setup
        setup();
		
        //Build request
        RestRequest request = new RestRequest();
        request.requestUri = endpoint;
        request.params.put('personal_number', '19910603-2890'); //Personal_number that is not connected to Contact in Salesforce
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = new RestResponse();
        
        ma_Employee.doGet();

		//Asserts
		System.assertEquals(404, RestContext.response.statusCode);    
    }
    
    /**
		Incorrect call. Returning 400 Bad request. Input parameter empty. 
    */
    @isTest
    public static void testGetEmployeeNoPersNumber(){
        
        //Setup
       	setup();
		
        //Build request
        RestRequest request = new RestRequest();
        request.requestUri = endpoint;
        request.params.put('personal_number', '');
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = new RestResponse();
        
        ma_Employee.doGet();

		//Asserts
		System.assertEquals(400, RestContext.response.statusCode);    
    }
    
    /**
		Incorrect call. Returning 400 Bad request. Input parameter missing in call. 
    */
    @isTest
    public static void testGetEmployeeNoParam(){
        
        //Setup
        setup();
		
        //Build request
        RestRequest request = new RestRequest();
        request.requestUri = endpoint;
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = new RestResponse();
        
        ma_Employee.doGet();

		//Asserts
		System.assertEquals(400, RestContext.response.statusCode);    
    }
    
    /**
		Correct call. Returning 200 Success + updating Contact languages. 
    */    
    @isTest
    public static void testPatchEmployeeCorrect(){
        //Setup
        setup();
		
        //Build request
        RestRequest request = new RestRequest();
        request.requestUri = endpoint;
        request.params.put('personal_number', con.Personal_Number__c);
        request.params.put('lang', 'en');
        request.httpMethod = 'PATCH';
        RestContext.request = request;
        RestContext.response = new RestResponse();
        
        ma_Employee.doPatch();

		//Asserts
		System.assertEquals(200, RestContext.response.statusCode);
        System.debug(con);
        Contact con2 = [SELECT id, languages__c from Contact where Personal_Number__c = :con.Personal_Number__c];
        System.debug(con2);
        
    }
    
    /**
		Incorrect call. Returning 404 Not found. Input personal number not tied to any employee in Salesforce.
    */
    @isTest
    public static void testPatchEmployeeNoContact(){
        //Setup
        setup();
		
        //Build request
        RestRequest request = new RestRequest();
        request.requestUri = endpoint;
        request.params.put('personal_number', '19910603-2890'); //Personal_number that is not connected to Contact in Salesforce
        request.params.put('lang', 'en312v31v2312v');
        request.httpMethod = 'PATCH';
        RestContext.request = request;
        RestContext.response = new RestResponse();
        
        ma_Employee.doPatch();

		//Asserts
		System.assertEquals(404, RestContext.response.statusCode);
        
    }
    
    /**
		Incorrect call. Returning 400 Bad request. Input parameter empty. 
    */
    @isTest
    public static void testPatchEmployeeNoPersNumber(){
        //Setup
        setup();
		
        //Build request
        RestRequest request = new RestRequest();
        request.requestUri = endpoint;
        request.params.put('personal_number', '');
        request.params.put('lang', 'en');
        request.httpMethod = 'PATCH';
        RestContext.request = request;
        RestContext.response = new RestResponse();
        
        ma_Employee.doPatch();

		//Asserts
		System.assertEquals(400, RestContext.response.statusCode);
        
    }

    /**
		Incorrect call. Returning 400 Bad request. Input parameter missing in call. 
    */
    @isTest
    public static void testPatchEmployeeNoParam(){
        //Setup
        setup();
		
        //Build request
        RestRequest request = new RestRequest();
        request.requestUri = endpoint;
        request.params.put('lang', 'en');
        request.httpMethod = 'PATCH';
        RestContext.request = request;
        RestContext.response = new RestResponse();
        
        ma_Employee.doPatch();

		//Asserts
		System.assertEquals(400, RestContext.response.statusCode);
        
    }


}
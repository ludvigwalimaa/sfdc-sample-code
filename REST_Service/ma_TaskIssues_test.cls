/**
    Test class for ma_TaskIssues.
	@author Ludvig Wälimaa - Deloitte

*/
@isTest
public class ma_TaskIssues_test {

    private static String endpoint;
    private static WorkOrder wo;
    private static WorkOrderLineItem woli;
    private static Contact emp;

    /**
     Setting up data.
   */
    private static void setup(){
        //Setup metadata
        TestMetaDataFactory.createProductMetaData();

        List<Area__c> areas = TestMetaDataFactory.createAreaMetaData();
        Area__c testArea = areas.get(0);
        testArea.Area_Manager__c = [SELECT id From User WHERE FirstName != null AND LastName != null AND IsActive = true LIMIT 1].Id;
        upsert testArea;
        System.debug('testArea' + testArea);
        //Create account
        List<Account> acc_list = TestDataFactory.createAccounts(1, 'Household');
        Account acc = acc_list.get(0);
        acc.Name = 'Firstname Lastname';
        insert acc;


        //Create Hemfridare (Test employee) and Collaborator
        List<Contact> con_list = new List<Contact>();
        emp = new Contact();
        emp.RecordTypeId = RecordTypeHelper.CONTACT_EMPLOYEE;
        emp.FirstName = 'TestFirstName';
        emp.LastName = 'TestLastName';
        emp.Area__c = testArea.Id;
        emp.Languages__c = 'sv;en';
        con_list.add(emp);

        insert con_list;

        System.debug('areamanager' + emp.Area__r.Area_Manager__r.Id);

        //Contract
        List<Contract> contr_list = [SELECT Id, AccountId, RecordTypeId, Account.Name, Pricebook2Id FROM Contract Where AccountId = :acc.Id];
        Contract contr = contr_list.get(0);
        contr.Account = acc;
        contr.Account.Name = acc.Name;
        upsert contr;
        contr = [SELECT Id, AccountId, RecordTypeId, Account.Name, Pricebook2Id FROM Contract Where AccountId = :acc.Id];

        //Address
        List<Delivery_Address__c> da_list = TestDataFactory.createDeliveryAddresses(acc_list, '12300', true);
        insert da_list;

        Delivery_Address__c da = da_list.get(0);

        //Date
        Date startDate = Date.newInstance(2017, 12, 25);

        //Order
        Id ordId;
        ordId = TestOrderFactory.createAndScheduleSubscriptionOrder(contr, acc , da.Id, startDate);

        List<Order> order_list = [SELECT Id, (SELECT id From Work_Orders__r) FROM Order where Id = :ordId];

        //WorkOrder
        List<WorkOrder> wo_list = order_list.get(0).Work_Orders__r;
        wo = wo_list.get(0);
        wo.Description = 'Desc';
        wo.Customer_Comment__c = 'Customer comment';
        upsert wo;


        //WorkOrderLineItem
        woli = [SELECT id FROM WorkOrderLineItem WHERE WorkOrderId = :wo.Id];
        woli.Employee__c = emp.Id;
        woli.Empl_Area_Id__c = emp.Area__c;
        woli.Description = 'Description';
        update woli;

        //Build endpoint
        setEndpoint();
    }

    /**
     Setting up data to trigger catch when Area doesn't have an Area manager.
     This triggers an error when trying to insert new Case cause the OwnerId is missing (is retrieved from Area.AreaManager).
     */
    private static void setupNoAreaManager(){
        //Setup metadata
        TestMetaDataFactory.createProductMetaData();
        List<Area__c> areas = TestMetaDataFactory.createAreaMetaData();
        Area__c testArea = areas.get(0);

        //Create account
        List<Account> acc_list = TestDataFactory.createAccounts(1, 'Household');
        Account acc = acc_list.get(0);
        acc.Name = 'Firstname Lastname';
        insert acc;


        //Create Hemfridare (Test employee)
        List<Contact> con_list = new List<Contact>();
        emp = new Contact();
        emp.RecordTypeId = RecordTypeHelper.CONTACT_EMPLOYEE;
        emp.FirstName = 'TestFirstName';
        emp.LastName = 'TestLastName';
        emp.Area__c = testArea.Id;
        emp.Languages__c = 'sv;en';
        con_list.add(emp);
        insert con_list;

        //Contract
        List<Contract> contr_list = [SELECT Id, AccountId, RecordTypeId, Account.Name, Pricebook2Id FROM Contract Where AccountId = :acc.Id];
        Contract contr = contr_list.get(0);
        contr.Account = acc;
        contr.Account.Name = acc.Name;
        upsert contr;
        contr = [SELECT Id, AccountId, RecordTypeId, Account.Name, Pricebook2Id FROM Contract Where AccountId = :acc.Id];

        //Address
        List<Delivery_Address__c> da_list = TestDataFactory.createDeliveryAddresses(acc_list, '12300', true);
        insert da_list;

        Delivery_Address__c da = da_list.get(0);

        //Date
        Date startDate = Date.newInstance(2017, 12, 25);

        //Order
        Id ordId;
        ordId = TestOrderFactory.createAndScheduleSubscriptionOrder(contr, acc , da.Id, startDate);

        List<Order> order_list = [SELECT Id, (SELECT id From Work_Orders__r) FROM Order where Id = :ordId];

        //WorkOrder
        List<WorkOrder> wo_list = order_list.get(0).Work_Orders__r;
        wo = wo_list.get(0);
        wo.Description = 'Desc';
        wo.Customer_Comment__c = 'Customer comment';
        upsert wo;


        //WorkOrderLineItem
        woli = [SELECT id FROM WorkOrderLineItem WHERE WorkOrderId = :wo.Id];
        woli.Employee__c = emp.Id;
        woli.Empl_Area_Id__c = emp.Area__c;
        woli.Description = 'Description';
        update woli;

        //Build endpoint
        setEndpoint();
    }

    /**
     Retrieves the base url of Salesforce and builds the full api url.
    */
    private static void setEndpoint(){
        //Get domain + apiurl.
        String domain = Url.getSalesforceBaseUrl().toExternalForm();
        String api = '/services/apexrest/Task/Issues';
        endpoint = domain + api;
    }

    /**
		Correct call. Returning 200 Success + json response.
    */
    @isTest
    public static void testPostTaskIssueCorrect() {
        //Setup
        setup();

        //Build request
        RestRequest request = new RestRequest();
        request.requestUri = endpoint;
        request.params.put('id', woli.Id);
        request.params.put('mime', 'png');
        request.params.put('imgName','testimg' );
        request.params.put('desc', 'test purpose');
        request.params.put('empId', emp.Id);
        request.httpMethod = 'POST';
        RestContext.request = request;
        RestContext.response = new RestResponse();


        ma_TaskIssues.doPost('iVBORw0KGgoAAAANSUhEUgAAABUAAAAaCAIAAADXI4AUAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAoSURBVDhPY/Qon8hAAWCC0uSCUf2UgVH9lIFR/ZSBUf2UgZGtn4EBADUeAYQfV8kAAAAAAElFTkSuQmCCi');

        //Assert
        System.assertEquals(201, RestContext.response.statusCode);

    }

    /**
		Incorrect call. Returning 400 Bad request. Input parameter missing.
    */
    @isTest
    public static void testPostTaskIssueNoParam() {
        //Setup
        setup();

        //Build request
        RestRequest request = new RestRequest();
        request.requestUri = endpoint;
        request.params.put('mime', 'png');
        request.params.put('imgName','testimg' );
        request.params.put('desc', 'test purpose');
        request.params.put('empId', emp.Id);
        request.httpMethod = 'POST';
        RestContext.request = request;
        RestContext.response = new RestResponse();


        ma_TaskIssues.doPost('iVBORw0KGgoAAAANSUhEUgAAABUAAAAaCAIAAADXI4AUAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAoSURBVDhPY/Qon8hAAWCC0uSCUf2UgVH9lIFR/ZSBUf2UgZGtn4EBADUeAYQfV8kAAAAAAElFTkSuQmCCi');

        //Assert
        System.assertEquals(400, RestContext.response.statusCode);

    }

    /**
		Incorrect call. Returning 400 Bad request. Input parameter empty.
    */
    @isTest
    public static void testPostTaskIssueNoId() {
        //Setup
        setup();

        //Build request
        RestRequest request = new RestRequest();
        request.requestUri = endpoint;
        request.params.put('id', '');
        request.params.put('mime', 'png');
        request.params.put('imgName','testimg' );
        request.params.put('desc', 'test purpose');
        request.params.put('empId', emp.Id);
        request.httpMethod = 'POST';
        RestContext.request = request;
        RestContext.response = new RestResponse();


        ma_TaskIssues.doPost('iVBORw0KGgoAAAANSUhEUgAAABUAAAAaCAIAAADXI4AUAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAoSURBVDhPY/Qon8hAAWCC0uSCUf2UgVH9lIFR/ZSBUf2UgZGtn4EBADUeAYQfV8kAAAAAAElFTkSuQmCCi');

        //Assert
        System.assertEquals(400, RestContext.response.statusCode);

    }

    /**
		Incorrect call. Returning 404 Not found. Input empId not tied to any employee in Salesforce.
    */
    @isTest
    public static void testPostTaskIssueNoEmployee() {
        //Setup
        setup();

        //Build request
        RestRequest request = new RestRequest();
        request.requestUri = endpoint;
        request.params.put('id', woli.Id);
        request.params.put('mime', 'png');
        request.params.put('imgName','testimg' );
        request.params.put('desc', 'test purpose');
        request.params.put('empId', '123');
        request.httpMethod = 'POST';
        RestContext.request = request;
        RestContext.response = new RestResponse();


        ma_TaskIssues.doPost('iVBORw0KGgoAAAANSUhEUgAAABUAAAAaCAIAAADXI4AUAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAoSURBVDhPY/Qon8hAAWCC0uSCUf2UgVH9lIFR/ZSBUf2UgZGtn4EBADUeAYQfV8kAAAAAAElFTkSuQmCCi');

        //Assert
        System.assertEquals(404, RestContext.response.statusCode);

    }

    /**
		Incorrect call. Returning 404 Not found. Input id not tied to any WorkOrderLineItem in Salesforce.
    */
    @isTest
    public static void testPostTaskIssueNoWoli() {
        //Setup
        setup();

        //Build request
        RestRequest request = new RestRequest();
        request.requestUri = endpoint;
        request.params.put('id', '123');
        request.params.put('mime', 'png');
        request.params.put('imgName','testimg' );
        request.params.put('desc', 'test purpose');
        request.params.put('empId', emp.Id);
        request.httpMethod = 'POST';
        RestContext.request = request;
        RestContext.response = new RestResponse();


        ma_TaskIssues.doPost('iVBORw0KGgoAAAANSUhEUgAAABUAAAAaCAIAAADXI4AUAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAoSURBVDhPY/Qon8hAAWCC0uSCUf2UgVH9lIFR/ZSBUf2UgZGtn4EBADUeAYQfV8kAAAAAAElFTkSuQmCCi');

        //Assert
        System.assertEquals(404, RestContext.response.statusCode);

    }

    /**
		Incorrect call. Returning 500 Internal Server Error.
		Trying to insert new Case cause the OwnerId is missing (is retrieved from Hemfridares AreaManager). This is against the rules in Salesforce.
    */
    @isTest
    public static void testPostTaskIssueNoCaseOwnerId() {
        //Setup
        setupNoAreaManager();

        //Build request
        RestRequest request = new RestRequest();
        request.requestUri = endpoint;
        request.params.put('id', woli.Id);
        request.params.put('mime', 'png');
        request.params.put('imgName','testimg' );
        request.params.put('desc', 'test purpose');
        request.params.put('empId', emp.Id);
        request.httpMethod = 'POST';
        RestContext.request = request;
        RestContext.response = new RestResponse();

        ma_TaskIssues.doPost('iVBORw0KGgoAAAANSUhEUgAAABUAAAAaCAIAAADXI4AUAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAoSURBVDhPY/Qon8hAAWCC0uSCUf2UgVH9lIFR/ZSBUf2UgZGtn4EBADUeAYQfV8kAAAAAAElFTkSuQmCCi');

        //Assert
        System.assertEquals(500, RestContext.response.statusCode);

    }
}
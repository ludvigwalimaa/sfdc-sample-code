/**
	Class for easily getting the record type id with close to compile time safety
	(New record types still need to be correctly conigured in this class to be supported)

	The class can be used directly as it is lazy initialized upon use by a static block;
	Example of use: RecordTypeHelper.ORDER_SINGLE

	@author Peter
*/
public without sharing class RecordTypeHelper {
	public static final Id ORDER_SINGLE;
	public static final Id ORDER_RUNNING;
	public static final Id ORDER_SUBSCRIPTION;
	public static final Id ORDER_PRODUCT_ORDER;
	public static final Id CONTACT_EMPLOYEE;
	public static final Id CONTACT_CUSTOMER;
	public static final Id ACCOUNT_BUSINESS;
	public static final Id ACCOUNT_HOUSEHOLD;
	public static final Id CASE_DAMAGE;
	public static final Id CASE_DETERGENT;
	public static final Id CONTRACT_BUSINESS;
	public static final Id CONTRACT_EMPLOYEE;
	public static final Id CONTRACT_FRAME;
	public static final Id CONTRACT_HOUSEHOLD;
	public static final Id WORKORDER_SERVICE;
	public static final Id WORKORDER_PRODUCT;
	public static final Id OPPORTUNITY_BABYSITTING;
	public static final Id OPPORTUNITY_CLEANINGSERVICESINGLE;
	public static final Id OPPORTUNITY_ENTERPRISE;
	public static final Id OPPORTUNITY_CLEANINGSERVICE;
	public static final Id OPPORTUNITY_OFFICE;
	public static final Id OPPORTUNITY_GARDEN;
	public static final Id OPPORTUNITY_WEEKDAYCARE;
	public static final Id WOLI_SERVICE;
	public static final Id WOLI_TRAVEL_FEE;
	public static final Id WOLI_PRODUCT_OPTION;
	public static final Id WOLI_CANCELATION_FEE;
	public static final Id WOLI_REBOOKING_FEE;
	public static final Id PRODUCT_PRODUCT_BUNDLE;
	public static final Id PRODUCT_PRODUCT;
  	public static final Id PROMOTION_TRADE_DISCOUNT;
  	public static final Id PROMOTION_CAMPAIGN;
	public static final Id	PROFORMA_INVOICE_CREDIT_ORDER;
	public static final Id	PROFORMA_INVOICE_SALES_ORDER;

	static {
		List<RecordType> rtList = [SELECT Id,SobjectType,DeveloperName FROM RecordType WHERE IsActive=true];
		for(RecordType rt:rtList){
			if(rt.SobjectType=='Order'){
				if(rt.DeveloperName=='Order')ORDER_SINGLE=rt.Id;
				if(rt.DeveloperName=='Running')ORDER_RUNNING=rt.Id;
				if(rt.DeveloperName=='Subscription')ORDER_SUBSCRIPTION=rt.Id;
				if(rt.DeveloperName=='Product_Order')ORDER_PRODUCT_ORDER=rt.Id;
			}
			if(rt.SobjectType=='Account'){
				if(rt.DeveloperName=='Business_account')ACCOUNT_BUSINESS=rt.Id;
				if(rt.DeveloperName=='Household')ACCOUNT_HOUSEHOLD=rt.Id;
			}
			if(rt.SobjectType=='Case'){
				if(rt.DeveloperName=='Damage')CASE_DAMAGE=rt.Id;
				if(rt.DeveloperName=='P_fyllning_av_medel')CASE_DETERGENT=rt.Id;
			}
			if(rt.SobjectType=='Contact'){
				if(rt.DeveloperName=='Customercontact')CONTACT_CUSTOMER=rt.Id;
				if(rt.DeveloperName=='Hemfridare')CONTACT_EMPLOYEE=rt.Id;
			}
			if(rt.SobjectType=='Contract'){
				if(rt.DeveloperName=='Business')CONTRACT_BUSINESS=rt.Id;
				if(rt.DeveloperName=='Employee')CONTRACT_EMPLOYEE=rt.Id;
				if(rt.DeveloperName=='Frame')CONTRACT_FRAME=rt.Id;
				if(rt.DeveloperName=='Household')CONTRACT_HOUSEHOLD=rt.Id;
			}
			if(rt.SobjectType=='WorkOrder'){
				if(rt.DeveloperName=='Service')WORKORDER_SERVICE=rt.Id;
				if(rt.DeveloperName=='Product')WORKORDER_PRODUCT=rt.Id;
			}
			if(rt.SobjectType=='WorkOrderLineItem'){
				if(rt.DeveloperName=='Service')WOLI_SERVICE=rt.Id;
				if(rt.DeveloperName=='Travel_Fee')WOLI_TRAVEL_FEE=rt.Id;
				if(rt.DeveloperName=='Product_Option')WOLI_PRODUCT_OPTION=rt.Id;
				if(rt.DeveloperName=='Cancelation_Fee')WOLI_CANCELATION_FEE=rt.Id;
				if(rt.DeveloperName=='Rebooking_Fee')WOLI_REBOOKING_FEE=rt.Id;
			}
			if(rt.SobjectType=='Opportunity'){
				if(rt.DeveloperName=='Babysitting')OPPORTUNITY_BABYSITTING=rt.Id;
				if(rt.DeveloperName=='CleaningService_OneTime')OPPORTUNITY_CLEANINGSERVICESINGLE=rt.Id;
				if(rt.DeveloperName=='Enterprise')OPPORTUNITY_ENTERPRISE=rt.Id;
				if(rt.DeveloperName=='CleaningService')OPPORTUNITY_CLEANINGSERVICE=rt.Id;
				if(rt.DeveloperName=='Office')OPPORTUNITY_OFFICE=rt.Id;
				if(rt.DeveloperName=='Garden')OPPORTUNITY_GARDEN=rt.Id;
				if(rt.DeveloperName=='Vardagsomsorg')OPPORTUNITY_WEEKDAYCARE=rt.Id;
			}
			if(rt.SobjectType=='Product2'){
				if(rt.DeveloperName=='Product_Bundle')PRODUCT_PRODUCT_BUNDLE=rt.Id;
				if(rt.DeveloperName=='Product')PRODUCT_PRODUCT=rt.Id;
			}
			if(rt.SobjectType=='Promotion__c'){
        if(rt.DeveloperName=='Trade_discount')PROMOTION_TRADE_DISCOUNT=rt.Id;
        if(rt.DeveloperName=='Campaign')PROMOTION_CAMPAIGN=rt.Id;
      }
			if(rt.SobjectType=='Proforma_Invoice__c'){
				if(rt.DeveloperName=='Credit_Order')PROFORMA_INVOICE_CREDIT_ORDER=rt.Id;
				if(rt.DeveloperName=='Sales_Order')PROFORMA_INVOICE_SALES_ORDER=rt.Id;
			}
		}
	}
}
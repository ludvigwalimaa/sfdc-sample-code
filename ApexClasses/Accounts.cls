/**
 * Created by nlundkvist on 2018-01-29.
 */

public class Accounts extends fflib_SObjectDomain {
  public static final String ACCOUNT_STATUS_ACTIVE = 'Active Customer';
  public static final String ACCOUNT_STATUS_INACTIVE = 'Inactive Customer';
  
  public static final String ACCOUNT_TYPE_CUSTOMER = 'Customer';
  public static final String ACCOUNT_TYPE_PROSPECT = 'Prospect';
  

  public static final String ACCOUNT_SUBSTATUS_TERMINATED = 'Terminated agreement';

  public static final String ACCOUNT_RELATION_GROUP = 'Group Agreement Relation';
  public static final String ACCOUNT_RELATION_COORDINATION = 'Coordination Agreement Relation';
  public static final String ACCOUNT_RELATION_COMPANY = 'Company Relation';

  public List<InvoicingError> invoicingErrors { get; private set; }

  private Set<Id> accountSkipIds = new Set<Id>();
  private Integer year;
  private Date startDate;
  private Date stopDate;
  private Boolean consultantFee;
  private Boolean fullYear;
  private Map<Id, List<PricingResponse>> periodPricesByPayerIds;
  private Map<Id, PricingResponse> fullYearPriceByAgreementLineIds;
  private Map<Id, Decimal> minimumPriceByApplicationAreaIds;

  public Accounts(List<Account> records){
    super(records);
  }

  public void initiateInvoicing(Set<Id> accountIds, Set<Id> agreementIds, Integer year, Date startDate, Date stopDate){
    invoicingErrors = new List<InvoicingError>();
    this.year = year;
    this.startDate = startDate;
    this.stopDate = stopDate;
    Map<Id, List<Agreement_Line__c>> agreementLinesByAccountIds = new AgreementLinesSelector().getAgreementLinesByPayerAccountIds(agreementIds, startDate, stopDate);
    Map<Id, Report_Line__c> reportLineByAgreementLineIds = new ReportsSelector().getAgreementReportLineByAgreementLineIds(agreementIds, startDate, stopDate);
    Map<Id, Schablon_Line__c> schablonLinesByAgreementLineIds = new SchablonsSelector().getUnreportedSchablonLinesByAgreementLineIds(agreementIds, startDate, stopDate);
    PricingService.PricingResponseFactory prf = new PricingService.PricingResponseFactory(startDate, stopDate);
    this.fullYear = prf.fullYear;
    PricingService.PricingResponseFactory fullYearPRF = new PricingService.PricingResponseFactory(Date.newInstance(year, 1, 1), Date.newInstance(year, 12, 31));
    Set<Id> applicationAreaIds = new Set<Id>();
    for (Account a : (List<Account>) Records){
      try {
        List<Agreement_Line__c> agreementLines = agreementLinesByAccountIds.get(a.Id);
        if (agreementLines==null || agreementLines.isEmpty()) throw new InvoicingException(InvoicingService.INVOICE_ERROR_NOTHING_TO_INVOICE);
        prf.add(agreementLines, reportLineByAgreementLineIds, schablonLinesByAgreementLineIds);
        fullYearPRF.add(agreementLines, false);
        if (!applicationAreaIds.contains(a.Application_Area__c)) applicationAreaIds.add(a.Application_Area__c);
      } catch (PricingException ex){
        accountSkipIds.add(a.Id);
        invoicingErrors.add(new InvoicingError(a.Id, null, ex.getMessage()));
      } catch (InvoicingException ex){
        accountSkipIds.add(a.Id);
        invoicingErrors.add(new InvoicingError(a.Id, null, ex.getMessage()));
      }
    }
    periodPricesByPayerIds = prf.getPricingResponsesByPayerIds();
    fullYearPriceByAgreementLineIds = fullYearPRF.getPricingResponseByAgreementLineIds();
    minimumPriceByApplicationAreaIds = new MinimumPricesSelector().getMinimumPriceByApplicationAreaIds(applicationAreaIds, year);
  }

  public void invoiceParentAccounts(fflib_ISObjectUnitOfWork uow){
    InvoicingService.InvoiceFactory invoiceFactory = new InvoicingService.InvoiceFactory(uow);
    for (Account a : (List<Account>) Records){
      if (accountSkipIds.contains(a.Id)) continue;
      Decimal minimumPrice = minimumPriceByApplicationAreaIds.containsKey(a.Application_Area__c) ?
        minimumPriceByApplicationAreaIds.get(a.Application_Area__c) : 0;
      try {
        List<PricingResponse> pricingResponses = periodPricesByPayerIds.get(a.Id);
        InvoicingService.Invoice invoice = new InvoicingService.Invoice(
          a.Id, a.Invoice_Footer_Text__c, startDate, stopDate, InvoicingService.INVOICE_TYPE_DEBIT
        );
        invoice.paymentTerms = 30;
        Decimal invoiceTotal = 0;
        Agreement_Line__c penaltyFeeAL;
        for (PricingResponse pr : pricingResponses){
          if (pr.schablonQuantity && penaltyFeeAL==null) penaltyFeeAL = pr.agreementLine;
          PricingResponse fullYearPR = fullYearPriceByAgreementLineIds.get(pr.agreementLineId);
          invoiceTotal += pr.price;
          invoice.invoiceLines.add(InvoicingService.createInvoiceLine(pr, fullYearPR));
        }
        if (invoice.invoiceLines.isEmpty()) throw new InvoicingException(InvoicingService.INVOICE_ERROR_NO_LINES_CREATED);
        if (penaltyFeeAL!=null) invoice.invoiceLines.add(InvoicingService.createPenaltyInvoiceLine(penaltyFeeAL, startDate, stopDate));
        if (minimumPrice > invoiceTotal){
          InvoicingService.InvoiceLine invoiceLine = InvoicingService.createMinimumAmountInvoiceLine(
            minimumPrice, pricingResponses[0].agreementLine,
            String.format('{0} {1}', new List<String>{a.BillingStreet, a.BillingCity}),
            startDate, stopDate, fullYear
          );
          invoiceTotal+=invoiceLine.price;
          invoice.minimumPrice = true;
          invoice.invoiceLines.add(invoiceLine);
        }
        if (invoiceTotal < InvoicingService.MINIMUM_INVOICE_AMOUNT_DEFAULT) throw new InvoicingException(InvoicingService.INVOICE_ERROR_BELOW_MINIMUM);
        invoiceFactory.add(invoice);
      } catch (InvoicingException ie){
        invoicingErrors.add(new InvoicingError(a.Id, null, ie.getMessage()));
      }
    }
  }

  public void setChangedSigningStatus(fflib_ISObjectUnitOfWork uow){
    for (Account a : (List<Account>) Records){
      if (!a.Prior_Agreement_Refusal__c){
        a.Prior_Agreement_Refusal__c = true;
        uow.registerDirty(a);
      }
    }
  }

  public void deactivateAccount(fflib_ISObjectUnitOfWork uow){
    for (Account a : (List<Account>) Records){
      if (a.Customer_Status__c != ACCOUNT_STATUS_INACTIVE){
        a.Customer_Status__c = ACCOUNT_STATUS_INACTIVE;
        a.Sub_Customer_Status__c = ACCOUNT_SUBSTATUS_TERMINATED;
        uow.registerDirty(a);
      }
    }
  }

  public void activateAccount(fflib_ISObjectUnitOfWork uow){
    for (Account a : (List<Account>) Records){
      if (a.Customer_Status__c != ACCOUNT_STATUS_ACTIVE){
        a.Type = ACCOUNT_TYPE_CUSTOMER;
        a.Customer_Status__c = ACCOUNT_STATUS_ACTIVE;
        a.Sub_Customer_Status__c = null;
        uow.registerDirty(a);
      }
    }
  }

  public class Constructor implements fflib_SObjectDomain.IConstructable {
    public fflib_SObjectDomain construct(List<SObject> records){
      return new Agreements(records);
    }
  }
}
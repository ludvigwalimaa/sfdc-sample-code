/**
 * Created by lwalimaa on 2018-01-11.
 */

@isTest
public with sharing class PushNotificationServiceMock implements HttpCalloutMock {
    protected Integer code;
    protected String status;
    protected String bodyAsString;
    protected Blob bodyAsBlob;
    protected Map<String, String> responseHeaders;

    public PushNotificationServiceMock(Integer code, String status, String body,Map<String, String> responseHeaders){
        this.code = code;
        this.status = status;
        this.bodyAsString = body;
        this.bodyAsBlob = null;
        this.responseHeaders= responseHeaders;
    }

    public HttpResponse respond(HTTPRequest req){
        HttpResponse resp = new HttpResponse();
        resp.setStatusCode(code);
        resp.setStatus(status);
        if(bodyAsBlob != null){
            resp.setBodyAsBlob(bodyAsBlob);
        }else{
            resp.setBody(bodyAsString);
        }
        if(responseHeaders != null){
            for(String key : responseHeaders.keySet()){
                resp.setHeader(key, responseHeaders.get(key));
            }
        }
        return resp;

    }

}
/**
 * Created by lwalimaa on 2018-01-11.
 */
@isTest (SeeAllData  = true)
public class TestPushNotificationService {

    private static Contact c;

    static void setup(){
        //c = [SELECT id FROM Contact LIMIT 1];
        c = new Contact(LastName = 'TestContact');
        insert c;
    }

    @isTest
    static void testPS204(){
        setup();

        Integer statusCode  = 204;
        String jsonResponse = '';
        String status = 'OK';

        PushNotificationServiceMock fakeResponse = new PushNotificationServiceMock(statusCode, status, jsonResponse, null);
        List<Id> contactIds = new List<Id>();
        contactIds.add(c.Id);

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        HttpResponse res = PushNotificationService.sendPush(contactIds, 'NewFeedback');
        PushNotificationService.sendPushFuture(contactIds, 'NewFeedback');
        Test.stopTest();
        System.assertEquals(204, res.getStatusCode());
    }
    @isTest
    static void testPS200(){
        setup();

        Integer statusCode  = 200;
        String jsonResponse = '';
        String status = 'OK';

        PushNotificationServiceMock fakeResponse = new PushNotificationServiceMock(statusCode, status, jsonResponse, null);
        List<Id> contactIds = new List<Id>();
        contactIds.add(c.Id);

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        HttpResponse res = PushNotificationService.sendPush(contactIds, 'NewFeedback');
        PushNotificationService.sendPushFuture(contactIds, 'NewFeedback');
        Test.stopTest();
        System.assertEquals(200, res.getStatusCode());

    }

}
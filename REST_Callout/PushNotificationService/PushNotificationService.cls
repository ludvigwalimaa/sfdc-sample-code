/**
 * Created by lwalimaa on 2018-01-11.
 */

public with sharing class PushNotificationService {

    private static String SMS_GATWAY_URL = 'callout:PushNotification/test/notifications/employee';

    @future(callout = true)
    public static void sendPushFuture(List<Id> pushIdList, String pushNotificationTemplate){
        sendPush(pushIdList, pushNotificationTemplate);
    }

    public static HttpResponse sendPush(List<Id> pushIdList, String pushNotificationTemplate){
        Push_Notification_Template__mdt pn  = [SELECT Tag__c, Message__c FROM Push_Notification_Template__mdt WHERE DeveloperName = :pushNotificationTemplate LIMIT 1];
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(SMS_GATWAY_URL);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Ocp-Apim-Subscription-Key', '{!$Credential.Password}');
        req.setBody(
            '{"' +
                'employees":' + JSON.serialize(pushIdList) + ', "' +
                'message": "' + pn.Message__c + '", "' +
                'tag":"' + pn.Tag__c +
            '"}'
        );
        HttpResponse resp = new HttpResponse();
        try{
            resp = http.send(req);
        }catch(Exception ex){
            System.debug(ex.getMessage());
        }

        if(resp.getStatusCode() == 204){
            //All good
            System.debug(resp.getBody());
            System.debug(resp.getStatusCode());

        }else if(resp.getStatusCode() == 200){
            //Partial fail, some Ids can't be found in EmpId-Device ID matrix and did therefore not receive a push notificaiton.
            System.debug(resp.getBody());
            System.debug(resp.getStatusCode());
        }
        return resp;
    }

}
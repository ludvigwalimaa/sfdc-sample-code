public with sharing class WarehouseCalloutService {

    private static final String WAREHOUSE_URL = 'https://th-superbadge-apex.herokuapp.com/equipment';
    
    // complete this method to make the callout (using @future) to the
    // REST endpoint and update equipment on hand.
    @future (callout = true)
    public static void runWarehouseEquipmentSync(){
        HttpRequest req = new HttpRequest();
        req.setEndpoint(WAREHOUSE_URL);
        req.setMethod('GET');
        
        HTTP h = new HTTP();
        HTTPResponse resp = h.send(req);
        List<ExternalProduct> ext_list = (List<ExternalProduct>) JSON.deserialize(resp.getBody(), List<ExternalProduct>.class);
        updateEquipment(ext_list);
        System.debug(ext_list.get(0).sku);
    }
    
    public static void updateEquipment(List<ExternalProduct> extProd){
        Map<Id, Product2> prodMap = new Map<Id, Product2>([SELECT Id, Name, Cost__c, Maintenance_Cycle__c, Lifespan_Months__c, Warehouse_SKU__c FROM Product2 WHERE Replacement_Part__c = true]);
        List<ExternalProduct> updateExtProd = new List<ExternalProduct>();
        for(ExternalProduct ep : extProd){
            for(Product2 p : prodMap.values()){
                if(p.Warehouse_SKU__c == ep.sku){
                    //p.Warehouse_SKU__c 		= ep.sku;
                    p.Lifespan_Months__c 	= ep.lifespan;
                    p.Maintenance_Cycle__c 	= ep.maintenanceperiod;
                    p.Cost__c 				= ep.cost;
                    p.Current_Inventory__c 	= ep.quantity;
                }
            }
        }
        upsert prodMap.values(); 
    }
    
    public static void createProducts(List<ExternalProduct> extProd){
        List<ExternalProduct> updateExtProd = new List<ExternalProduct>();
        List<Product2> prods = new List<Product2>();
        for(ExternalProduct ep : extProd){
                Product2 p 				= new Product2();
                p.Name 					= ep.name;
                p.Warehouse_SKU__c 		= ep.sku;
                p.Lifespan_Months__c 	= ep.lifespan;
                p.Maintenance_Cycle__c 	= ep.maintenanceperiod;
                p.Cost__c 				= ep.cost;
                p.Current_Inventory__c 	= ep.quantity;
            	p.IsActive 				= true;
                prods.add(p);
            }
        insert prods;
    }
    
    public class ExternalProduct{
        public Boolean replacement;
        public Integer quantity;
        public String name;
        public Integer maintenanceperiod;
        public Integer lifespan;
        public Integer cost;
        public String sku;
	}
}